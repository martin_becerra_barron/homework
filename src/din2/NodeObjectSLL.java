package din2;
public class NodeObjectSLL {
    public Object info;
    public NodeObjectSLL next;
    
    public NodeObjectSLL(){
       next = null;
    }
    public NodeObjectSLL(Object el, NodeObjectSLL ptr){
       info = el;
       next = ptr;
    }
}
