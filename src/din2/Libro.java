package din2;
public class Libro {
    private String titulo;
    private String autor;
    private int year;
    public Libro(String t, String a, int y){
       this.titulo=t;
       this.autor=a;
       this.year=y;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    public String tostring(){
        return titulo+","+autor+","+year;
    }
    public boolean equals(Object other){
         Libro libro = (Libro)other;
         return libro.getAutor().equals(this.autor) &&
                libro.getTitulo().equals(this.titulo) &&
                libro.getYear() == this.year;
    }
    
}
