/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dinamicas;

/**
 *
 * @author Martin Becerra
 */
public class profesol {
    private String palabra;
    private int frecuencia;

    public profesol(String nom, int num){
        this.palabra=nom;
        this.frecuencia=num;
    }
    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public int getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(int frecuencia) {
        this.frecuencia = frecuencia;
    }
    
    public String toString(){
        return (this.palabra+" : "+this.frecuencia);
    }
    
    
    
}
