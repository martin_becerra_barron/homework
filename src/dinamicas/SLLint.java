package dinamicas;

import java.io.FileInputStream;

public class SLLint {
    NodeSLLInt head;
    NodeSLLInt tail;    
    
    
    SLLint(){
       head=tail=null;
    }    
    boolean isEmpty(){
        return head == null;
    }
    
   public void addToHead(int el){
         if  ( !isEmpty()   ){
               head = new NodeSLLInt(el, head);
         }else{
             head = tail = new NodeSLLInt(el);
         }
    }
    
    public void addToTail(int el){
           if (!isEmpty()){
              tail.next = new NodeSLLInt(el);
              tail = tail.next;
           }else{
               head = tail = new NodeSLLInt(el);
           }
    }
    
    public int deleteFromHead(){
         int el = head.info;
         if(head == tail)
             head=tail=null;
         else
             head = head.next;
         return el;
    }
    
    public int deleteFromTail(){
        int el = tail.info;
         if(tail == head){
              head = tail = null;
         }
         else{
             NodeSLLInt tmp;
             for(
              tmp =head; tmp.next != tail  ; tmp = tmp.next      
                     );
             tail=tmp;
             tail.next=null;
         } return el;
    }
    
    
    
    
    public void printAll(){
        for(NodeSLLInt tmp = head; tmp != null; tmp=tmp.next)
               System.out.println(tmp.info + "  ");
            }
    
   public boolean isInList(int el){
        NodeSLLInt tmp;
        for(tmp = head; tmp != null && tmp.info !=el;tmp = tmp.next);
        return tmp != null;
   } 
    
}
