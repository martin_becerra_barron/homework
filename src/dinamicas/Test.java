package dinamicas;

import java.util.Scanner;

public class Test {

    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        String anuncio = "Dime cuantos numeros quieres ingresar en la lista ?";
        System.out.print(anuncio+" ");
        int k = Integer.parseInt(sc.nextLine());
        SLLint ed = new SLLint();
        int n=0;
        for(int i=0;i<k;i++){
           String pregunta = "dame el elemento "+(i+1)+": "; 
           System.out.print(pregunta);
           n = Integer.parseInt(sc.nextLine());
           ed.addToTail(n);
        }
        anuncio = "Los numeros en tu lista son: ";
        System.out.println(anuncio);
        ed.printAll();
    }
}
