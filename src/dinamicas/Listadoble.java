/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dinamicas;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Martin Becerra
 */
public class Listadoble {
    Nod cabeza;
    Nod cola;
    public Listadoble(){
        cabeza=null;
        cola=null;
    }
    public boolean vacia(){
        if(cabeza==null){
            return (true);
        }else{
            return(false);
        }
    }
    
    public void incertarprimero(String dato){
        Nod tmp=new Nod(dato);
        if(vacia()){
            cabeza=tmp;
            cola=tmp;
        }else{
            tmp.Siguiente=cabeza;
            cabeza.Previo=tmp;
            cabeza=tmp;
        }
    }
    public void incertarfinal(String dato){
        Nod tmp=new Nod(dato);
        if(vacia()){
            cabeza=tmp;
            cola=tmp;
        }else{
            cola.Siguiente=tmp;
            tmp.Previo=cola;
            cola=tmp;
        }
    }
    public void borrarprimero(){
        cabeza=cabeza.Siguiente;
        cabeza.Previo=null;
    }
    public void borrarfinal(){
        if(cola.Previo==null){
            cabeza=null;
            cola=null;
        }else{
            cola=cola.Previo;
            cola.Siguiente=null;
        }
    }
    public String listar(){
        String Dato="";
        Nod aux=cabeza;
        while(aux!=null){
            Dato+="["+ aux.info+"}";
            aux=aux.Siguiente;
        }
        return Dato;
    }
    
    
    public void printAll(){
        Nod tmp;
        for(tmp = cabeza; tmp !=null; tmp = tmp.Siguiente){
           System.out.print(tmp.info);   
           System.out.println();
           //if(nombre==tmp.info.getNombre())
           
        }
    }  
    
    public void creartxt(String dato){
        File f;
        int cont=0;
        FileWriter escritoArchivo;
        Nod tmp;
        PrintWriter salida;
        BufferedWriter bw;
        try{
            f=new File(dato);
            escritoArchivo=new FileWriter(f);
            bw=new BufferedWriter(escritoArchivo);
            salida=new PrintWriter(bw);
            for(tmp=cabeza;tmp!=null;tmp=tmp.Siguiente){
            cont++;
                salida.write(tmp.info+"\n");                
            
            
            }
            String cam=Integer.toString(cont);
            salida.write("total: "+cam);
            
            salida.close();
            bw.close();
        }catch(IOException e){
            System.out.println();
        }
    }
}
